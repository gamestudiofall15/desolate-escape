﻿using UnityEngine;
using System.Collections;

public class Dataless : MonoBehaviour {

	/* Only See Light when patrolling
	 * If player steps on glass, dataless begins patrol, if player happens to step on glass later on in the game, Dataless Patrols faster
	 * Modes: Yellow (Passive Searching) Red (Spotted, Game Over)
	 * Once Alerted, Dataless begins patrolling for entirety of game
	 * Behind Camera
	 * */ 

	// Use this for initialization
	
	public float eyeSpeed; 

	public Rigidbody rb;

	void Start () {
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		rb.velocity = new Vector3 (eyeSpeed, 0, 0);
		//if (other.tag == "MoveBack" )

	}


	void OnTriggerEnter(Collider other)
	{
		
		if (other.tag == "Player")
		{
			Destroy(other.gameObject);

			
		}
			

}
}
