﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    private Rigidbody rb;

    public float jumpSpeed;
	public float secondJumpSpeed;
	public float regularSpeed;
	public float runSpeed;
	public float crouchSpeed;
	public float slideSpeed;
	public float slideDuration;

	private float speed;

    private bool grounded = true;
    private bool run = true;
    private bool crouch;
	private bool secondJump;

	private int direction;


	private Vector3 regularScale;
	private Vector3 crouchScale;
	private Vector3 slideScale;

	private PlayerStates playerState;

	public enum PlayerStates {
		Idle,
		Walk,
		Run,
		Jump,
		Climb,
		Slide,
		Crouch,
	};

    void Start () {
		playerState = PlayerStates.Idle;
        rb = GetComponent<Rigidbody>();
        run = false;
        crouch = false;
		speed = regularSpeed;

		regularScale = transform.localScale;
		crouchScale = transform.localScale - new Vector3(0, 0.5f, 0);
		slideScale = transform.localScale - new Vector3(-0.3f, 0.3f, 0);

	}

    void Update()
    {

		if (Input.GetAxisRaw ("Horizontal") > 0) {
			direction = 1;
		} 
		else if (Input.GetAxisRaw ("Horizontal") < 0) {
			direction = -1;
		}

		Debug.Log (playerState);

		switch (playerState) {
		case PlayerStates.Idle:
			IdleUpdate ();
			break;
		case PlayerStates.Walk:
			WalkUpdate ();
			break;
		case PlayerStates.Run:
			RunUpdate ();
			break;
		case PlayerStates.Jump:
			JumpUpdate ();
			break;
		case PlayerStates.Climb:
			ClimbUpdate ();
			break;
		case PlayerStates.Slide:
			SlideUpdate ();
			break;
		case PlayerStates.Crouch:
			CrouchUpdate ();
			break;
		default:
			break;
		}
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
		grounded = true;
		if (playerState == PlayerStates.Jump) { 
			playerState = PlayerStates.Idle;
		}
    }

	private void IdleUpdate() {
		if (Input.GetKeyDown(KeyCode.Space) && grounded == true)
		{
			Jump (); 
		}

		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			Run ();
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyDown(KeyCode.LeftControl))
		{
			Crouch ();
		}

		if (Input.GetKeyUp (KeyCode.LeftControl)) 
		{
			StopCrouch();
		}
	}

	private void WalkUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)  && grounded == true)
		{
			Jump (); 
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			Run ();
		}

		if (Input.GetKeyDown(KeyCode.LeftControl))
		{
			Crouch ();
		}

		if (Input.GetKeyUp (KeyCode.LeftControl)) 
		{
			StopCrouch();
		}

		if (Input.GetAxisRaw ("Horizontal") == 0) 
		{
			playerState = PlayerStates.Idle;
		}

	}

	private void RunUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)  && grounded == true)
		{
			Jump (); 
		}

		if (Input.GetKeyUp(KeyCode.LeftShift))
		{
			StopRun ();
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyDown(KeyCode.F)) 
		{
			Slide ();
		}
	}

	private void JumpUpdate() {
		if (Input.GetKeyDown(KeyCode.Space) && secondJump == false)
		{
			SecondJump (); 
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}
	}

	private void ClimbUpdate() {

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}
	}

	private void SlideUpdate() {

	}

	private void CrouchUpdate() {
		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyUp (KeyCode.LeftControl)) 
		{
			StopCrouch();
		}
	}

	private void Jump() {
		rb.AddForce(0, jumpSpeed, 0);
		grounded = false;
		playerState = PlayerStates.Jump;
		secondJump = false;
	}

	private void SecondJump() {
		rb.AddForce(0, secondJumpSpeed, 0);
		secondJump = true;
	}

	private void Walk() {
		transform.Translate(speed * Input.GetAxis("Horizontal") * Time.deltaTime, 0f, 0f);
		if ((playerState != PlayerStates.Jump) && (playerState != PlayerStates.Crouch) && (playerState != PlayerStates.Run) && (playerState != PlayerStates.Slide)) {
			playerState = PlayerStates.Walk;
		}
	}

	private void Run() {
		speed = runSpeed;
		transform.Translate(speed * Input.GetAxis("Horizontal") * Time.deltaTime, 0f, 0f);
		playerState = PlayerStates.Run;
	}

	private void StopRun() {
		speed = regularSpeed;
		playerState = PlayerStates.Walk;
	}

	private void Crouch() {
		playerState = PlayerStates.Crouch;
		transform.localScale = crouchScale;
		speed = crouchSpeed;
	}

	private void StopCrouch() {
		transform.localScale = regularScale;
		speed = regularSpeed;

		if(Input.GetAxisRaw ("Horizontal") != 0) {
			playerState = PlayerStates.Walk;
		}
		else {
			playerState = PlayerStates.Idle;
		}

	}

	private void Slide() {
		rb.AddForce (slideSpeed * direction, 0, 0);
		Debug.Log (slideSpeed * direction);
		playerState = PlayerStates.Slide;
		transform.localScale = slideScale;
		Invoke ("StopSlide", slideDuration);
	}

	private void StopSlide () {
		playerState = PlayerStates.Idle;
		transform.localScale = regularScale;
	}


}